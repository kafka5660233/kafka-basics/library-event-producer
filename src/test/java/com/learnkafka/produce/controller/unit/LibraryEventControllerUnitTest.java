package com.learnkafka.produce.controller.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.produce.controller.LibraryEventController;
import com.learnkafka.produce.domain.Book;
import com.learnkafka.produce.domain.LibraryEvent;
import com.learnkafka.produce.domain.LibraryEventType;
import com.learnkafka.produce.producer.LibraryEventProducer;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(value = { LibraryEventController.class })
public class LibraryEventControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private LibraryEventProducer mockLibraryEventProducer;


    @Test
    void postLibraryEvent_approach1() throws Exception {
        String path = "/v1/libraryevent/approach1";
        // given
        LibraryEvent request = new LibraryEvent(
                123,
                LibraryEventType.NEW,
                new Book(456, "unit-test-book","unit-test-author")
        );

        Mockito.when(mockLibraryEventProducer.sendLibraryEvent_approach1_async(Mockito.any(LibraryEvent.class)))
                .thenReturn(null);
        String jsonRequest = objectMapper.writeValueAsString(request);
        // when
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(path)
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isCreated());
        // then
    }

    @Test
    void postLibraryEvent_approach1_invalidRequest_return_400() throws Exception {
        String path = "/v1/libraryevent/approach1";
        // given
        LibraryEvent invalidRequest = getInvalidRequest();

        Mockito.when(mockLibraryEventProducer.sendLibraryEvent_approach1_async(Mockito.any(LibraryEvent.class)))
                .thenReturn(null);
        String jsonRequest = objectMapper.writeValueAsString(invalidRequest);
        // when and then
        String expectedErrMsg = "book.bookId - must not be null, book.bookName - must not be blank";
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(path)
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.content().string(expectedErrMsg));
    }

    /*
     *
     * Book id CAN NOT be null as it's annotated as @NotNull
     * Book name CAN NOT be empty as it's annotated as @NotBlank
     */
    private static LibraryEvent getInvalidRequest() {
        return new LibraryEvent(
                123,
                LibraryEventType.NEW,
                new Book(null, "", "unit-test-author")
        );
    }
}
