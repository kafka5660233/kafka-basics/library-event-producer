package com.learnkafka.produce.controller.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.produce.domain.Book;
import com.learnkafka.produce.domain.LibraryEvent;
import com.learnkafka.produce.domain.LibraryEventType;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@EmbeddedKafka(
        topics = "${spring.kafka.topic}",
        partitions = 3
)
class LibraryEventControllerIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    @Autowired
    private ObjectMapper objectMapper;

    private Consumer<Integer, String> consumer;


    @BeforeEach
    void setUp() {
        Map<String, Object> configs = new HashMap<>(KafkaTestUtils.consumerProps("group1", "true", embeddedKafkaBroker));
        configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        // initialising kafka consumer
        consumer = new DefaultKafkaConsumerFactory<>(configs, new IntegerDeserializer(), new StringDeserializer())
                .createConsumer();

        // wiring / attaching consumer to embedded kafka broker
        embeddedKafkaBroker.consumeFromAllEmbeddedTopics(consumer);
    }

    @AfterEach
    void tearDown() {
        consumer.close();
    }

    @Test
    void postLibraryEvent_approach1() {
        String path = "/v1/libraryevent/approach1";
        // given
        LibraryEvent request = new LibraryEvent(
                123,
                LibraryEventType.NEW,
                new Book(456, "int-test-book","int-test-author")
        );

        // when
        HttpHeaders headers = new HttpHeaders();
        headers.add("content-type", MediaType.APPLICATION_JSON.toString());
        HttpEntity<LibraryEvent> requestHttpEntity = new HttpEntity<>(request, headers);
        var responseENtity = testRestTemplate.exchange(
                path,
                HttpMethod.POST,
                requestHttpEntity,
                LibraryEvent.class
                );

        // then
        try {
            assertRecord(responseENtity, request);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        };
    }


    @Test
    void postLibraryEvent_approach2() {
        String path = "/v1/libraryevent/approach2";
        // given
        LibraryEvent request = new LibraryEvent(
                123,
                LibraryEventType.NEW,
                new Book(456, "int-test-book","int-test-author")
        );

        // when
        HttpHeaders headers = new HttpHeaders();
        headers.add("content-type", MediaType.APPLICATION_JSON.toString());
        HttpEntity<LibraryEvent> requestHttpEntity = new HttpEntity<>(request, headers);
        var responseENtity = testRestTemplate.exchange(
                path,
                HttpMethod.POST,
                requestHttpEntity,
                LibraryEvent.class
        );

        // then
        try {
            assertRecord(responseENtity, request);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void postLibraryEvent_approach3_asKafkaRecord() {
    }

    private void assertRecord(ResponseEntity<LibraryEvent> responseENtity, LibraryEvent request) throws JsonProcessingException {
        assertEquals(HttpStatus.CREATED, responseENtity.getStatusCode());
        ConsumerRecords<Integer, String> consumerRecords = KafkaTestUtils.getRecords(consumer);
        // this below  commented line is failing when we run all test as "mvn clean install"
        // assert consumerRecords.count() == 1;

        for (ConsumerRecord<Integer, String> consumerRecord : consumerRecords) {
            System.out.println("##################################################" + consumerRecords.count());
            LibraryEvent libraryEventActual = objectMapper.readValue(consumerRecord.value(), LibraryEvent.class);
            assertEquals(request.libraryEventId(), libraryEventActual.libraryEventId());
        }
    }
}