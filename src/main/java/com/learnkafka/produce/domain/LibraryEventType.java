package com.learnkafka.produce.domain;

public enum LibraryEventType {
    NEW,
    UPDATE
    ;
}
