package com.learnkafka.produce.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.learnkafka.produce.domain.LibraryEvent;
import com.learnkafka.produce.producer.LibraryEventProducer;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@Slf4j
@RequiredArgsConstructor
public class  LibraryEventController {

    private final LibraryEventProducer libraryEventProducer;

    @PostMapping(path =  "/v1/libraryevent/approach1")
    public ResponseEntity<LibraryEvent> postLibraryEvent_approach1(
            @Valid @RequestBody LibraryEvent libraryEvent
    ) throws JsonProcessingException {
        log.info("library event : {} " , libraryEvent);
        libraryEventProducer.sendLibraryEvent_approach1_async(libraryEvent);
        log.info("log to verify ASYNC , as it will be print **BEFORE** send-fail or send-success  of libraryEventProducer.sendLibraryEvent_approach1_async");
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(libraryEvent);
    }

    @PostMapping(path =  "/v1/libraryevent/approach2")
    public ResponseEntity<LibraryEvent> postLibraryEvent_approach2(
            @RequestBody LibraryEvent libraryEvent
    ) throws JsonProcessingException, ExecutionException, InterruptedException {
        log.info("library event : {} " , libraryEvent);
        libraryEventProducer.sendLibraryEvent_approach2_sync(libraryEvent);
        log.info("log to verify SYNC , as it will be print **AFTER** send-fail or send-success  of libraryEventProducer.sendLibraryEvent_approach1_async");
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(libraryEvent);
    }

    @PostMapping(path =  "/v1/libraryevent/approach3")
    public ResponseEntity<LibraryEvent> postLibraryEvent_approach3_asKafkaRecord(
            @RequestBody LibraryEvent libraryEvent
    ) throws JsonProcessingException, ExecutionException, InterruptedException {
        log.info("library event : {} " , libraryEvent);
        libraryEventProducer.sendLibraryEvent_approach3_async_asKafkaRecord(libraryEvent);
        log.info("log to verify ASYNC , as it will be print **BEFORE** send-fail or send-success  of libraryEventProducer.sendLibraryEvent_approach1_async");
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(libraryEvent);
    }
}
