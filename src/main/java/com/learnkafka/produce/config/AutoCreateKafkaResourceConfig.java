package com.learnkafka.produce.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class AutoCreateKafkaResourceConfig {

    @Value(value = "${spring.kafka.topic}")
    private String topic;

    /*
    if we have NewTopic type bean then this the specific
    topic auto created if it does not ALREADY CREATED

    RECOMMENDED FOR DEV ENV ONLY
     */
    @Bean
    @Profile(value = {"local"})
    public NewTopic libraryEvents(){
        return TopicBuilder
                .name(topic)
                .partitions(3)
                .build();
    }
}
