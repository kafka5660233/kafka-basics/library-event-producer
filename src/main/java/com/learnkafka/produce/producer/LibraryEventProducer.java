package com.learnkafka.produce.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.produce.domain.LibraryEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
@Slf4j
public class LibraryEventProducer {

    @Value(value = "${spring.kafka.topic}")
    private String topic;
    private final KafkaTemplate<Integer, String> kafkaTemplate;

    private final ObjectMapper objectMapper;

    public LibraryEventProducer(KafkaTemplate<Integer, String> kafkaTemplate, ObjectMapper objectMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
    }

    public CompletableFuture<SendResult<Integer, String>> sendLibraryEvent_approach1_async(LibraryEvent libraryEvent) throws JsonProcessingException {
        Integer key = libraryEvent.libraryEventId();
        String value = objectMapper.writeValueAsString(libraryEvent);
        /*
        1.  For the
            first call after this app bootstraps BLOCKING call to kafka cluster to get kafka topic metadata
            from second call it never call kafka cluster again
         */
        // 2.  send message call is always ASYNC and returns CompletableFuture
        var completableFuture = kafkaTemplate
                .send(topic, key, value);

        completableFuture.whenComplete((sendResult, throwable)->{
            if(!Objects.isNull(throwable)){
                handleFailure(key, value, throwable);
            } else {
                handleSuccess(key, value, sendResult);
            }
        });

        return completableFuture;
    }

    public void sendLibraryEvent_approach2_sync(LibraryEvent libraryEvent) throws JsonProcessingException, ExecutionException, InterruptedException {
        Integer key = libraryEvent.libraryEventId();
        String value = objectMapper.writeValueAsString(libraryEvent);

        /*
        1 . For the
            first call after this app bootstraps BLOCKING call to kafka cluster to get kafka topic metadata
            from second call it never call kafka cluster again
         */
        // 2.  send message call is also SYNC since we called .get on CompletableFuture
        var sendResult = kafkaTemplate
                .send(topic, key, value)
                .get();
        handleSuccess(key, value, sendResult);
    }

    // [MOST_RECOMMENDED]
    // as we can send headers(i.e metadata) with kafka message
    public void sendLibraryEvent_approach3_async_asKafkaRecord(LibraryEvent libraryEvent) throws JsonProcessingException, ExecutionException, InterruptedException {
        Integer key = libraryEvent.libraryEventId();
        String value = objectMapper.writeValueAsString(libraryEvent);

        /*
        1 . For the
            first call after this app bootstraps BLOCKING call to kafka cluster to get kafka topic metadata
            from second call it never call kafka cluster again
         */
        // 2.  send message call is also SYNC since we called .get on CompletableFuture
        ProducerRecord<Integer, String> pRecord = buildProducerRecord(key, value);
        var completableFuture = kafkaTemplate
                .send(pRecord);
        completableFuture.whenComplete((sendResult, throwable)->{
            if(!Objects.isNull(throwable)){
                handleFailure(key, value, throwable);
            } else {
                handleSuccess(key, value, sendResult);
            }
        });
    }

    private ProducerRecord<Integer, String> buildProducerRecord(Integer key, String value) {
        List<Header> recordHeaders = List.of(
                new RecordHeader("key1", "value1".getBytes(StandardCharsets.UTF_8)),
                new RecordHeader("key2", "value1".getBytes(StandardCharsets.UTF_8))
        );
        ProducerRecord<Integer, String> producerRecord = new ProducerRecord<>(
                topic,
                null,
                key,
                value,
                recordHeaders);

        // Adding custom headers
        producerRecord.headers()
                .add(new RecordHeader("key3", "value3".getBytes(StandardCharsets.UTF_8)))
                .add(new RecordHeader("key4", "value4".getBytes(StandardCharsets.UTF_8)));
        return producerRecord;
    }

    private void handleSuccess(Integer key, String value, SendResult<Integer, String> sendResult) {
        log.info(
                """
                Message sent successfully,
                key : {}
                value : {}
                topic : {}
                partition: {}
                offset : {}
                headers(metadata) : {}
                """,
                key, value,
                sendResult.getRecordMetadata().topic(), sendResult.getRecordMetadata().partition(),
                sendResult.getRecordMetadata().offset(), sendResult.getRecordMetadata()
        );
    }

    private void handleFailure(Integer key, String value, Throwable throwable) {
        log.error("Error sending the message, cause :{}", throwable.getMessage(), throwable);
    }
}
